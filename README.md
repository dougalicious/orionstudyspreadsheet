
**Resources:** 
*Libraries*
* [UXtendProject](https://script.google.com/corp/home/projects/1dlt3RWSKBYTHbrLfKWacbgFfjyIIG33BVAQz7u_oLv3fXfn-iUmPIIkv)
* [IncentivesLibrary](https://script.google.com/corp/home/projects/1toPvXCA1lIDyGyhFAei5dP3i1zMEevjN7bM77YxW9tqD_UkL6jK5UK-d)
* [OrionLibraryV2](https://script.google.com/corp/home/projects/1xZ1r8SYZepq5LhnwC8NWuuj3B1K-qHrsgmS_VVdBJjTjVEJIb51kmMYh)

**Contents**

[TOC]

*Notes:*

*The original version of this script's features was availble via "add-on", this current version has moved away from that as additional security procedures have been instituded which would not make any updates feasible, along with long lead time for security review. Therefore this script has been done using "Libraries" feature withing Google Apps Script.*
*The Study templates reference 3 libraries of which each has a specific scope*
 * UXtendProject - Used to create "Day-Of Study" sheet for UXtend team
 * Incentives Library - Used by the process incentives from the study template, there's also an independent [UX Incentive Template - 2021](https://docs.google.com/spreadsheets/d/1ojZ6wzUX8g9yFpjY7rGPK39l9Warj1OppvWkft4euys/edit#gid=140392909) which shares this library
 * **OrionLibraryV2** - core functionality used for Study Template sheet
* 


## Objective {#objective}

* Replace existing "add-on" script to handle updates
* Facilite automation through the recurietment process

## Background {#background}

*came from jgolenbock@*

## Overview {#overview}

* [UXtendProject](https://script.google.com/corp/home/projects/1dlt3RWSKBYTHbrLfKWacbgFfjyIIG33BVAQz7u_oLv3fXfn-iUmPIIkv) - *the majority of the code used can be found in "UXtend Menu.gs" file. This consist of createUXtendTab, and refreshUxtendTab. These will call the corresponding library functions*
  * This library also contains the function to convert Participant time zone as a spreadsheet formula and can be found in file "formula.gs"
* [IncentivesLibrary](https://script.google.com/corp/home/projects/1toPvXCA1lIDyGyhFAei5dP3i1zMEevjN7bM77YxW9tqD_UkL6jK5UK-d) - *the majority of the code used can be found in "Incentives Menu.gs" file. This consist of processIncentives, importIncentivesFromSS, getIncentivesVersion. These will call the corresponding library functions*

* [OrionLibraryV2](https://script.google.com/corp/home/projects/1xZ1r8SYZepq5LhnwC8NWuuj3B1K-qHrsgmS_VVdBJjTjVEJIb51kmMYh) - *the majority of the code used can be found in "Orion Updates Menu.gs" file. This consist of moveToShortlist, moveToSchedule, markNotAFit, checkSignatures, checkOrionVersion, perksCapCheck, addPRQtoShortlistAndSchedule. These will call the corresponding library functions*

## Detailed Design {#design}

*The libraries have been organized relative to the .gs file extension names, which are all under the fn onOpen, which displays them as menu options*

## Project Information {#project}

*Please provide the following information about the project:*

external Links
* [UXtend HTML template](https://docs.google.com/document/d/198M3_9aGeHs46uFr2DygwolUWxKB3DRgvcP7nSEufy8/edit?resourcekey=0-9Lh_78SnR_oo4gd7a1I5WA)
* [UXtend Tab Template](https://docs.google.com/spreadsheets/d/1ZpTgyuR0EFBLWiK-0mEyvq-BtYu0ikvkWfUa30EQf9U/edit#gid=1434907922)
* [Staffing Trix](https://docs.google.com/spreadsheets/d/1ZoGLcVi_s7rZQgUx-Gan8rwK89yTTQsbuJm1BqTF1Vc/edit#gid=85027790)

## Dependency Considerations {#dependencies}

*As the Google Apps Script of this trix references 3 different libraries, any changes made to the sheets have to potential to impact existing libraries. When making updates to any of the libraries if there's a schema, sheet template layout changes both the associating library and the spreadsheet would need the update*

*Not a direct dependency* - however when an update on the spreadsheet's sheets' layout is updated the following trixes id's are usually updated to*
[Orion StudySS Tabs CP](https://script.google.com/corp/home/projects/1G_58PmO_UD_F0k3eR5Uxbc_8xdJmcFFfL7jBf9Lj_qgGQYXzGvztO_Gg/edit)
```
const idList = [
    "1_2CPU_cY2OdzB_1evih5Qf51rKQov_sYoVDK0sxiYJY",
    "1dqAkWZrB110pjm9Vk6ia81QPbjg0ty3pDCBMo_KmOFw",
    "1MFjsnDGqUgbK-x15Hw8QaACh5ZwJYQIlIXbLiKYuCEo",
    "1SCLm-_zPVuyPxxwksJwXx6EnsXEOwul2jcyPTOPzGDI",
    "1VO2PH8CfazIXdroSI-SXFcAhKhZbo6UUDDZ1T7Agrgs",
    "1UjBzwhFcDS2ONakaQqMBgKRdQNaW22APxoQKPaYv1wA",
    "1rpb2XJ-DhMEkK-8yJGMF_Of5hmqUEXe2GcctxGNSX0E", 
    "1zqbB_24v47gtXpaZXGnr4iWJTvqOmOki946csKFwxbI",  
    "1_M1XQeKrnecSVDJ37oRFK1OWWb2FwYIgBxLwgdV_ikA",
    "1bGJpWAWgw9c_G_hnHe4lCwEC9Ec1wn8vr1SV7_XRFg8"
  ]
  ```

## Document History {#document_history}

**Date** | **Author** | **Description** | **Reviewed by** | **Signed off by**
-------- | ---------- | --------------- | --------------- | -----------------
20210817      | douglascox@        | setup README | ...             | ...
...      | ...        | Dummy entry for | ...             | ...