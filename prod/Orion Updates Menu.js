function moveToShortlist() {
  try {
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    OrionLibraryV2.moveToShortListFromResponses(spreadsheet, Browser)
  } catch (e) {
    Browser.msgBox(`Unable to access Orion Library e: ${e}`)
  }
}

function moveToSchedule() {
  try {
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    OrionLibraryV2.moveToScheduleFromShortlist(spreadsheet, Browser)
  } catch (e) {
    Browser.msgBox(`Unable to access Orion Library e: ${e}`)
  }
}

function markNotAFit() {
  try {
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    OrionLibraryV2.markNotAFit(spreadsheet, Browser)
  } catch (e) {
    Browser.msgBox(`Unable to access Orion Library e: ${e}`)
  }
}

function checkSignatures() {
  try {
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    OrionLibraryV2.checkSignatures(spreadsheet, Browser)
  } catch (e) {
    Browser.msgBox(`Unable to access Orion Library e: ${e}`)
  }
}

function checkOrionVersion() {
  try {
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    OrionLibraryV2.checkVersion(spreadsheet, Browser)
  } catch (e) {
    Browser.msgBox(`Unable to access Orion Library e: ${e}`)
  }
}

function perksCapCheck() {
  try {
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    OrionLibraryV2.capCheckerByEmail(spreadsheet, Browser)
  } catch (e) {
    Browser.msgBox(`Unable to access Orion Library e: ${e}`)
  }
}


function addPRQtoShortListAndSchedule() {
  try {
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    OrionLibraryV2.addPRQtoShortListAndSchedule(spreadsheet, Browser)
  } catch (e) {
    Browser.msgBox(`Unable to access Orion Library e: ${e}`)
  }
}