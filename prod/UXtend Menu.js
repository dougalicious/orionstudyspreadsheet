function createUxtendTab() {
  let lock = LockService.getScriptLock();
  try {
    // Wait for up to 10 seconds for other processes to finish.
    lock.waitLock(10000);
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    UXtendProject.createUXtendTab(spreadsheet, Browser)
    lock.releaseLock();
    SpreadsheetApp.flush()
  } catch (e) {
    lock.releaseLock();
    Browser.msgBox(`Unable to access UXtend Library e: ${e}`)
  }
}

function refreshUxtendTab() {
  let lock = LockService.getScriptLock();
  try {
    lock.waitLock(10000);
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
    UXtendProject.updateUXtendTab(spreadsheet, Browser)
    lock.releaseLock();
  } catch (e) {
    lock.releaseLock();
    Browser.msgBox(`Unable to access UXtend Library e: ${e}`)
  }
  SpreadsheetApp.flush()
}

function getUXtendVersion() {
  try{
  Browser.msgBox(UXtendProject.getUXtendVersion())
  } catch (e){
    Browser.msgBox(`Unable to access UXtend Library e: ${e}`)
  }
}


