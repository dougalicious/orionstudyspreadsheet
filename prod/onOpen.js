function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Options')
    .addSubMenu(SpreadsheetApp.getUi().createMenu('Study Spreadsheet')
      // .addItem('Get Availability', 'getAvailablity')
      // 5/28 included in 'moveToShortlist' funtionality
      // .addItem('Move Questions to ShortList/Schedule', 'addPRQtoShortListAndSchedule')
      .addItem('Move to Shortlist', 'moveToShortlist')
      .addItem('Mark Not a Fit', 'markNotAFit')
      .addItem('Move to Schedule', 'moveToSchedule')
      .addItem('Check ICF', 'checkSignatures')
      .addItem('Incentives cap', 'perksCapCheck')
      .addItem('Check Library Version', 'checkOrionVersion'))
    .addSeparator()
    .addSubMenu(SpreadsheetApp.getUi().createMenu("Incentives")
      .addItem('Process Incentives', 'processIncentives')
      .addItem('Import Incentives from Spreadsheet', 'importIncentiveFromSS')
      .addItem('Check Incentives Version', 'getIncentivesVersion'))
    .addSeparator()
    .addSubMenu(SpreadsheetApp.getUi().createMenu('Uxtend Option')
      .addItem('Create Day-Of Study Tab', 'createUxtendTab')
      .addItem('Refresh Day-Of Study Tab', 'refreshUxtendTab')
      .addItem('Check Library UXtend Version', 'getUXtendVersion'))
    .addToUi();
}