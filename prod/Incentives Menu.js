function processIncentives() {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = ss.getSheetByName("Incentives");
  const name = sheet.getName()
  let sessionEmail = Session.getActiveUser().getEmail();
  try {
    typeof IncentivesLibrary === 'object' && IncentivesLibrary.processIncentives({ sheet, sessionEmail })
    SpreadsheetApp.flush()
  } catch (e) {
    Browser.msgBox("Unable to access Incentives Library e: ${e}")
  }

}

function importIncentiveFromSS() {
  try {
    const ss = SpreadsheetApp.getActiveSpreadsheet()
    const sheet = ss.getActiveSheet();
    IncentivesLibrary.importIncentives(sheet)
    SpreadsheetApp.flush()
  } catch (e) {
    Browser.msgBox("Unable to access Incentives Library e: ${e}")
  }

}

function getIncentivesVersion() {
  try {
    let versionNum = IncentivesLibrary.getIncentiveVersion();
    Browser.msgBox(`Incentives Version #${versionNum}`)
  } catch (e) {
    Browser.msgBox("Unable to access Incentives Library e: ${e}")
  }
}